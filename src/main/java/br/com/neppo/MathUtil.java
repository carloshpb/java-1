package br.com.neppo;

import java.util.ArrayList;

public class MathUtil {

	/**
	 * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
	 * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma
	 * dos seus elementos seja igual a "sum"
	 *
	 * @param ints
	 *            Conjunto de inteiros
	 * @param sum
	 *            Soma para o subconjunto
	 * @return
	 * @throws IllegalArgumentException
	 *             caso o argumento "ints" seja null
	 */

	public static boolean subsetSumChecker(int ints[], int sum) throws Exception {

		if (ints == null) {
			throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
		}

		if (sum == 0) {
			return true;
		}
		if ((ints.length == 0 && sum != 0) || sum < 0) {
			return false;
		}

		int ints2[] = new int[ints.length - 1];

		for (int i = 0; i < ints2.length; i++) {
			ints2[i] = ints[i];
		}

		if (ints[ints.length - 1] > sum) {
			return subsetSumChecker(ints2, sum);
		}

		return (subsetSumChecker(ints2, sum - ints[ints.length - 1]) || subsetSumChecker(ints2, sum));
	}
}